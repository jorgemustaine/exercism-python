def hello(name=''):
    if name != '':
        return "Hello, %s!" % name
    else:
        return "Hello, World!"

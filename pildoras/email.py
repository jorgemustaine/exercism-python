# -*- coding: utf-8 -*-
#!/usr/bin/env python

def solicitar():
    while True:
        mail_addr = raw_input('enter email address :')
        valida = valida_mail(mail_addr)
        if valida == True:
            print 'FIN'
            return False

def valida_mail(mail_addr):
    if len(mail_addr) == 0:
        print 'The email address can not be empty'
        return False
    elif mail_addr[0] == '@' or mail_addr[-1]=='@':
        print 'The email address can not start or end with "@"'
        return False
    elif mail_addr.count('@') == 0:
        print 'The email address the have at "@"'
        return False
    elif mail_addr.count('@') > 1:
        print 'The email address can not have more than one "@"'
        return False
    return True

solicitar()

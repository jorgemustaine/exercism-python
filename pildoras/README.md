# Python String Exercise

Exercise from [Formation video](https://www.youtube.com/watch?v=zH0VsRuD2ok&index=33&list=PLU8oAlHdN5BlvPxziopYZRd55pdqFwkeS)

### Ejercicio 1

Crea un programa que pida una dirección de email por teclado. el programa debe imprimir en consola si la dirección de email es correcta o no en función de si esta tiene el símbolo '@'. Si tiene una '@' la dirección será correcta. Si tiene más de una '@' o ninguna la dirección será incorrecta. Si la '@' está al comienzo de la dirección de email o al final,también se considera erronea.

def to_rna(dna_strand):
    dic = {'A': 'U', 'C': 'G', 'T': 'A', 'G': 'C'}
    rna_strand = ''

    for i in dna_strand:
        rna_strand=rna_strand+dic[i]

    return rna_strand

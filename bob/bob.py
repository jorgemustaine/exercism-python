def hey(phrase):
    phrase = phrase.strip()
    if len(phrase) == 0:
        return 'Fine. Be that way!'
    if phrase.endswith('?') and not phrase.isupper():
        return 'Sure.'
    if phrase.isupper() and phrase[-1] != '?':
        return 'Whoa, chill out!'
    if phrase[-1] == '?' and phrase == phrase.upper():
        return "Calm down, I know what I'm doing!"
    return 'Whatever.'

from collections import Counter

# Score categories
# Change the values as you see fit
def yacht(dice):
    return lambda dice: 50 if (dice.count(4) == 5) else 0

def ones(dice):
    return dice.count(1)

def twos(dice):
    return dice.count(2)*2

def threes(dice):
    return dice.count(3)*3

def fours(dice):
    return dice.count(4)*4

def fives(dice):
    return dice.count(5)*5

def sixes(dice):
    return dice.count(6)*6

def full_house(dice):
    if sorted(tuple(Counter(dice).values()))==[2, 3]:
       return sum(dice)
    return 0

def four_of_a_kind(dice):
    for i, k in Counter(dice).items():
        if 4 <= k:
            return k*4
    return 0


def little_str(dice):
    if sum(dice)==15:
        return 30
    return 0

def big_str(dice):
    if sum(dice)==20:
        return 30
    return 0

def choice(dice):
    return sum(dice)


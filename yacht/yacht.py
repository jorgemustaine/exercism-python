from collections import Counter

def four_of_a_kind(dice):
    for k, v in Counter(dice).items():
        if 4 <= v:
            return k * 4
    return 0
# Score categories
# Change the values as you see fit
YACHT = lambda dice: 50 if len(dice) == 5 and len(set(dice)) == 1 else 0
ONES = lambda dice: dice.count(1)
TWOS = lambda dice: dice.count(2)*2
THREES = lambda dice: dice.count(3)*3
FOURS = lambda dice: dice.count(4)*4
FIVES = lambda dice: dice.count(5)*5
SIXES = lambda dice: dice.count(6)*6
FULL_HOUSE = lambda dice: sum(dice) if \
        sorted(tuple(Counter(dice).values()))==[2, 3] else 0
FOUR_OF_A_KIND = four_of_a_kind
LITTLE_STRAIGHT = lambda dice: 30 if sum(dice)==15 else 0
BIG_STRAIGHT = lambda dice: 30 if sum(dice)==20 else 0
CHOICE = lambda dice: sum(dice)

def score(dice, category):
    return category(dice)

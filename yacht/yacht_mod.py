from calculos import *

def score(dice, category):
    switcher = {
        'YACHT': calculos.yacht,
        'ONES': calculos.ones,
        'TWOS': calculos.twos,
        'THREES': calculos.threes,
        'FOURS': calculos.fours,
        'FIVES': calculos.fives,
        'SIXES': calculos.sixes,
        'FULL_HOUSE': calculos.full_house,
        'FOUR_OF_A_KIND': calculos.four_of_a_kind,
        'LITTLE_STRAIGTH': calculos.little_str,
        'BIG_STRAIGTH': calculos.big_str,
        'CHOICE': calculos.choice,
    }
    func = switcher.get(category, lambda: 'Categoria erronea')
    return func(dice)

print(score([1,1,1,2,5], 'FULL_HOUSE'))
